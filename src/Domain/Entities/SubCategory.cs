﻿using CleanArchitecture.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace CleanArchitecture.Domain.Entities
{

    public class SubCategory : AuditableEntity
    {
        public int Id { get; set; }
        public string SubCategoryName { get; set; }
        public virtual Category Category { get; set; }

    }
}
